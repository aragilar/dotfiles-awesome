-- {{{ Rules
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = true,
                     keys = clientkeys,
                     buttons = clientbuttons } },
    --
    { rule = { class = "Iceweasel" },
      properties = { tag = "web" } },
    { rule = { class = "Firefox" },
      properties = { tag = "web" } },
    { rule = { class = "firefox" },
      properties = {
          tag = "web",
          floating = false,
          maximized = false,
      }
    },
    { rule = { class = "Miro.real"},
      properties = { tag = "vodcasts" } },
    { rule = { class = "Pidgin"},
      properties = { tag = "im" } },
    --
    { rule = { class = "gimp" },
      properties = { floating = true } },
    { rule = { name = "Qalculate!"},
      properties = { floating = true },
      callback = awful.client.setslave },
    { rule = { name = "SuperTuxCart"},
      properties = {
          floating = true,
          width = 1024,
          height = 768,
    }, },
    { rule = { class = "Tabletop Simulator.x86_64" },
      properties = { floating = true } },
}
-- }}}
