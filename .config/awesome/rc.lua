-- Standard awesome library
gears = require("gears")
awful = require("awful")
--tyrannical = require("tyrannical")
require("awful.autofocus")
-- Widget and layout library
wibox = require("wibox")
-- Remote execution
awful.remote = require("awful.remote")
-- Theme handling library
beautiful = require("beautiful")
-- Notification library
naughty = require("naughty")
menubar = require("menubar")
hotkeys_popup = require("awful.hotkeys_popup")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
-- }}}

constants = require("constants") -- loads constants
tags_mod = require("tags") -- loads tags
menu = require("menu") -- loads menu
taskbar = require("taskbar") -- loads taskbar
mouse_bindings = require("mouse_bindings") -- loads mouse bindings
key_bindings = require("key_bindings") -- loads key bindings
rules = require("rules") -- loads rules
signals = require("signals") -- loads signals
startup = require("startup") -- starts applications
